#!/bin/bash
#
#SBATCH --job-name=Transformer
#SBATCH --output=../batch_output/IWSLT14-Transformer-output.txt
#SBATCH --ntasks=1
#SBATCH --mem=10000
#SBATCH --mail-user=kotzaneck@cl.uni-heidelberg.de
#SBATCH --mail-type=ALL
#SBATCH --time=2-10:00:00

cd ../

# Add ICL-Slurm binaries to path
PATH=/opt/slurm/bin:$PATH
PATH=/usr/local/cuda-10.1//bin:$PATH
MANPATH=/usr/local/cuda-10.1//man:$MANPATH
LD_LIBRARY_PATH=/usr/local/cuda-10.1/lib64:$LD_LIBRARY_PATH
CUDA_VISIBLE_DEVICES=0 

python3.8 -m joeynmt train configs/transformer_iwslt14_deen_bpe.yaml


