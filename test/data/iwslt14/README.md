# Data of the iswlt14 data.

Source: https://wit3.fbk.eu/2014-01

It is alread encoded using the "scripts/get_iwslt14_bpe.sh" script.

To use it, look at the example of "configs/iwslt14_deen_bpe.yaml" configuration file.